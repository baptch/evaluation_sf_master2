<?php

namespace App\DataFixtures;

use App\Entity\Questions;
use App\Entity\Answers;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AnswersFixture extends Fixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
      $faker = Faker\Factory::create('fr_FR');
      $questions = $manager->getRepository(Question::class)->findAll();

      $count = 0;
      while ($count < 10) {
          $reponse = new Answers;
          $reponse->setStatus($faker->boolean(50));
          $reponse->setContent($faker->text(200));

          foreach($question as $questions){
              $reponse->setQuestionId($question->getId());
          }

          $manager->persist($reponse);
          $count++;
      }
      $manager->flush();
  }
  function getOrder()
  {
      return 3;
  }
}
