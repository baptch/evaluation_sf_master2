<?php

namespace App\DataFixtures;

use App\Entity\Users;
use App\Entity\Questions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class QuestionsFixture extends Fixture implements OrderedFixtureInterface
{
  public function load(ObjectManager $manager)
  {
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $users = $manager->getRepository(Users::class)->findAll();

        $count = 0;
        while ($count < 10) {
            $question = new Questions;
            $question->setTitle($faker->title);
            $question->setContent($faker->text(200));

            foreach($user as $users){
                $question->setUserId($user->getId());
            }

            $manager->persist($question);
            $count++;
        }
        $manager->flush();
    }
    function getOrder()
    {
        return 2;
    }
}
